import Model from '@mind-core-vue/model'

class Modelo extends Model {
	constructor() {
		super({
			uri:'/api/admin/modulos',
			key: 'id',
			template: {
				id: null,
				mostrarAcceso: null,
				perfiles: null,
				version: null,
				descripcion: null,				
			}
		})
		this.refreshTimeout = 3600
	}
}

export default new Modelo()