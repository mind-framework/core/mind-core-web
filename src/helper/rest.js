import Vue from 'vue';


/**
 * @description Librería para el manejo de tareas CRUD vía Rest
 * 
 */
export default class {
	/**
	 * @description constructor
	 * @param {string} uri uri base para las funciones REST
	 * @param {Object} state objeto que almacenará los resultados de las consultas
	 * @param {string} authUrl url del servicio de autenticación, por defecto '/auth'
	 */
	constructor({uri, state, key, authUrl= '/auth'}) {
		this.uri = uri
		if(state) {
			if(Array.isArray(state) || typeof state != 'object') {
				throw new Error('API: el paremetro `state` debe ser un objeto')
			}
			//si el state no tiene la pripiedad RESOURCE_LIST la establecemos
			if(!state.RESOURCE_LIST) {
				Vue.set(state, 'RESOURCE_LIST',[])
			}
			this.state = state
		}
		this.authUrl = authUrl || '/auth'
		this.resourceKey = key || 'id'
	}
	/**
	 * @description Realiza una consulta REST con la api especificada en el parámetro `uri` del objeto
	 * TODO: reconocer respuestas que no son json en los casos de error
	 * TODO: agregar soporte para limit y offset
	 * TODO: agregar soporte para groupby
	 */
	async FetchAll({filters, sort}={}) {
		// console.log(filters)
		let params = []
		if(filters) {
			for(const k of Object.keys(filters)) {
				const filter = filters[k]
				if(filter == null || filter==undefined) {
					continue
				}

				if(Array.isArray(filter)) {
					for(const f of filter) {
						params.push(`${k}=${f}`)
					}
				} else {
					params.push(`${k}=${filter}`)
				}
			}
		}
		if(sort) {
			for(const k of sort) {
				params.push(`sort=${k}`)
			}
		}

		const response = await fetch(`${this.uri}?${params.join('&')}`,{
			redirect: "manual"
		})
		if(response.redirected) {
			location.reload();
			return;
		}
		const responseText = await response.text()
		let json
		try {
			json = JSON.parse(responseText)
		} catch(e) {
			json = {message: responseText}
		}


		if(!response.ok) {
			switch(response.status) {
				case 302:
					location.reload();
					break;
				case 401:
					location.href = this.authUrl;
					break;
			}
			const msg = `Error al obtener datos de ${this.uri}: ${json.message}`
			throw msg
		}
		if(this.state) {
			this.state.RESOURCE_LIST = json.data
		}
		return json.data
	}

	/**
	 * @description Elimina el recurso especificado, realizando una consulta REST
	 * @param {*} id ID del recurso a eliminar
	 * TODO: reconocer respuestas que no son json en los casos de error
	 */
	async Delete(id) {
		if (typeof this.beforeDelete == 'function') {
			const resultadoHook = await this.beforeDelete()
			if(resultadoHook === false) {
				return
			}
		}

		if(typeof id == 'object') {
			id = id[this.resourceKey]
		}
		if(!id) {
			const msg = `Error Interno: no se encontró valor para resourceKey`
			throw msg
		}


		const uri = `${this.uri}/${id}`

		const response = await fetch(uri , {
			method: 'DELETE',
			redirect: "manual"
		})
		const json = await response.json()
		if(response.redirected) {
			location.reload();
		}
		if(!response.ok) {
			const msg = `Error al eliminar ${uri}: ${json.message}`
			throw msg
		}

		if(this.state) {
			const index = this.state.RESOURCE_LIST.findIndex(element => element[this.resourceKey] == id)
			if(index >= 0) {
				this.state.RESOURCE_LIST.splice(index,1)
			}

		}

	}

	/**
	 * @description Realiza una consulta REST con la api especificada en el parámetro `uri` del objeto
	 * TODO: reconocer respuestas que no son json en los casos de error
	 * TODO: buscar el recurso en RESOURCE_LIST antes de hacer el fetch
	 */
	async Fetch(id) {
		let recurso = `${this.uri}/${id}`
		const response = await fetch(recurso,{
			redirect: "error"
		})
		if(response.redirected) {
			location.reload();
		}

		const responseText = await response.text()
		let json
		try {
			json = JSON.parse(responseText)
		} catch(e) {
			json = {message: responseText}
		}


		if(!response.ok) {
			const msg = `Error al obtener recurso ${recurso}: ${json.message}`
			throw msg
		}
		if(!response.ok) {
			switch(response.status) {
				case 302:
					location.reload();
					break;
				case 401:
					location.href = this.authUrl;
					break;
			}
			const msg = `Error al obtener datos de ${this.uri}: ${json.message}`
			throw msg
		}
		// console.log(this.state.RESOURCE_LIST)
		return json.data

		// this.state.RESOURCE_LIST = json.data
	}

	/**
	 * Envía un comando PATCH a la api, con los campos modificados
	 * @param {Object} resource datos a enviar
	 * TODO: buscar el elemento en RESOURCE_LIST y comparar cambios
	 */
	async Patch(resource, id){		
		return new Promise((resolve, reject)=>{
			id = id || resource[this.resourceKey]
			if(!id) {
				const msg = `Error Interno: no se encontró valor para resourceKey`
				reject(msg)
			}
	
			const uri = `${this.uri}/${id}`
			fetch(uri , {
				method: 'PATCH',
				redirect: "manual",
				body: JSON.stringify(resource)
			})
			.then(async response => {
				if(response.redirected) {
					location.reload();
				}

				const text = await response.text()
				let data
				try {
					data = JSON.parse(text)
				} catch {
					reject(text)
					return
				}

				if(!response.ok) {
					reject(`${data.message}`)
				}
				return data.data
			})
			.then(data => {
				resolve(data)
				const id = data[this.resourceKey]
				const index = this.state.RESOURCE_LIST.findIndex(element => element[this.resourceKey] == id)

				if(index >= 0) {
					this.state.RESOURCE_LIST.splice(index,1, data)
				}


			})
			.catch((error)=>{
				reject(error)
			})
		})

	}

		/**
	 * Envía un comando POST a la api, para la creación de un nuevo recurso
	 * @param {Object} resource datos a enviar
	 */
	async Post(resource, { uri } = {}){
		return new Promise((resolve, reject)=>{
			const _uri =  uri || `${this.uri}`
			const payload = Array.isArray(resource) ? resource : [resource]
	

			fetch(_uri , {
				method: 'POST',
				redirect: "manual",
				body: JSON.stringify(payload)
			})
			.then(async response => {
				const text = await response.text()
				let data
				try {
					data = JSON.parse(text)
				} catch {
					reject(text)
					return
				}

				if(!response.ok) {
					reject(`${data.message}`)
				}
				return data.data
			})
			.then(data => {
				if(this.state) {
					this.state.RESOURCE_LIST = this.state.RESOURCE_LIST || []
					if(Array.isArray(data)){
						this.state.RESOURCE_LIST.push(...data)
					} else {
						this.state.RESOURCE_LIST.push(data)
					}
				}
				resolve(data)
			})
			.catch((error)=>{
				reject(error)
			})


		})

	}

}