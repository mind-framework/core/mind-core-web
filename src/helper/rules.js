export default {
	required: { required: true, message: 'Debe completar este campo', trigger: 'blur' },
	noWhitespace: { whitespace: true, message: 'Debe completar este campo', trigger: 'change' },
}