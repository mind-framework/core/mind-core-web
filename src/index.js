//Importar Mixines
import * as mixins from './mixins'
import Rules from './helper/rules'
import Rest  from './helper/rest'

export {
	mixins
}

export {
	Rest,
	Rules
}


//Importar componentes
import mindApp from './components/mind-app'
import mindLayout from './components/mind-layout'
import mindLayoutTable from './components/mind-layout-table'
import mindTable from './components/mind-table'
import mindForm from './components/mind-form'
import mindDashboard from './components/mind-dashboard'
import mindWidgetContainer from './components/mind-widget-container'
import mindWidgetIndicatorText from './components/widgets/indicator-text'

/**
 * Instala todos los componentes 
 * @param {Vue} Vue Referencia a Objeto Vue
 */
function install (Vue) {
	// Registro los componentes
	Vue.component('mindApp', mindApp)
	Vue.component('mindLayout', mindLayout)
	Vue.component('mindLayoutTable', mindLayoutTable)
	Vue.component('mindTable', mindTable)
	Vue.component('mindForm', mindForm)
	Vue.component('mindDashboard', mindDashboard)
	Vue.component('mindWidgetContainer', mindWidgetContainer)
	Vue.component('mindWidgetIndicatorText', mindWidgetIndicatorText)
}


export default {
	install,
}