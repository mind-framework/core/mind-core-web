	export default {
		mixins: [
		],
		created() {
			console.log('create')
		},
		beforeMount() {
			console.log('beforeMount')
		},
		async mounted() {
			if(!this.APIBaseURI) {
				console.error('se debe definir la variable this.APIBaseURI con la url de la API a consumir')
			}
		},
		beforeDestroy() {
			console.log('beforeDestroy')
		},
		data() {
			return {
				esNuevo: false,
				IS_LOADING: true,
				RESOURCE_LIST: [],
				RESOURCE: {},
				// APIBaseURI: ''

			}
		},
		computed: {
		},
		methods: {
			async FETCH_ALL({uri} = {}) {
			uri = uri || this.APIBaseURI  
			const response = await fetch(uri)
			if(!response.ok) {
				const msg = `Error al obtener datos de ${uri}`
				this.$message({
					type: 'error',
					message: msg
				});
				throw msg
				// return null
			}
			const json = await response.json()
			return json.data
			},

			/**
			 * Inicializa el recurso nuevo
			 */
			NEW() {
				this.esNuevo = true
				// this.recurso = JSON.parse(JSON.stringify(this.RESOURCE))
			},

			/** dispara la acción FETCH de vuex y realiza una copia local del recurso en this.recurso 
			 * 
			*/
			FETCH() {
				// return new Promise((resolve, reject) => {
				// 	this.$store.dispatch(`${vuexNamespace}/FETCH`, params).then((response) => {
				// 		this.recurso = JSON.parse(JSON.stringify(this.RESOURCE))
				// 		this.esNuevo = false
				// 		resolve(response)
				// 	}).catch((error) => {
				// 		this.$notify({
				// 			type: 'error',
				// 			title: this.titulo,
				// 			text: `${error.message}`
				// 		})
				// 		reject(error)
				// 	})
				// })
			},

			/**
			 * Hook que se ejecuta antes enviar la acción DELETE a vuex
			 * @returns {Boolean}	true = continuear con la eliminación
			 * 						false = cancelar eliminación
			 */
			async beforeDelete() {
				return true
			},

			/**
			 * @description 
			 * @param {*} id Referencia al Objeto a eliminar
			 */
			async REST_DELETE(uri) {
				if (typeof this.beforeDelete == 'function') {
					const resultadoHook = await this.beforeDelete()
					if(resultadoHook === false) {
						return
					}
				}

				if(!this.APIBaseURI) {
					this.$message({
						type: 'error',
						message: `Error Interno: no se definió la URI de la API`
					});
					return
				}

				// const uri = `${this.APIBaseURI}/${id}`

				fetch(uri, {
						method: 'DELETE',
					})
					.then(response => {
						if(!response.ok) {
							this.$message({
								type: 'success',
								message: `Se eliminó el registro`
							});
						}
						return response.json()
					})
					.then(json => {
						this.RESOURCE_LIST = json.data
					})
					.catch(error => { 
						this.$message({
							type: 'error',
							message: `Ocurrió un error al intentar eliminar '${error}'`
						});
					})
			},


			/**
			 * Hook que se ejecuta antes enviar la acción FETCH o POST a vuex
			 * @returns {Boolean}	true = continuear con el envío
			 * 						false = cancelar el envío
			 * USO:
			 * 	sobreescribir esta función en la sección methods del componente
			 */
			async beforeSubmit() {
				return true
			},

			/**
			 * Envía los datos al servidor
			 */
			async SUBMIT() {
				// if (typeof this.beforeSubmit == 'function' && await this.beforeSubmit() === false) {
				// 	return
				// }
				// if (this.esNuevo) {
				// 	this.POST([this.recurso])
				// 	this.camposModificados = Object.assign({});
				// }
				// else {
				// 	this.PATCH(this.recurso)
				// 	this.camposModificados = Object.assign({});
				// }
			},

			/**
			 * Hook que se ejecuta luego de enviar la acción FETCH o POST a vuex
			 * @params {Object} response contiene la respuesta del servidor
			 * USO:
			 * 	sobreescribir esta función en la sección methods del componente
			 */
			afterSubmit(response) {
				return response
			},

			/**
			 * Envía una acción POST a vuex API
			 * @param {Object} recurso 
			 */
			async POST(recurso) {
				recurso = recurso || this.recurso
				if (!recurso) {
					throw new Error(this.$t('vuexapi.errorResource'))
				}

			},

			/**
			 * Envía una acción PATCH a vuex API
			 * @param {Object} recurso 
			 */
			async PATCH(recurso) {
				recurso = recurso || this.recurso
				if (!recurso) {
					throw new Error('no se definió el recurso a enviar')
				}
			},

		},
		async beforeRouteLeave(to, from, next) {
			// if (Object.keys(this.camposModificados).length > 0 && this.SolicitarConfirmacionSalida) {

			// 	const answer = await this.$confirm(`${this.$t("msg.leavePage")}`, {
			// 		buttonTrueText: this.$t("msg.confirmTrue"),
			// 		buttonFalseText: this.$t("msg.confirmFalse"),
			// 		title: 'Warning'
			// 	})
			// 	if (answer) {
			// 		next()
			// 	} else {
			// 		next(false)
			// 	}
			// 	return;
			// }
			next()
		}
	}
